# -*- coding: utf-8 -*-
"""
vars.py -- Tools to implement interactions with VARS
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import requests
from libs.logger import LOG


def pull_all_concepts(url='http://m3.shore.mbari.org/kb/v1/concept', timeout=5):
    try:
        r = requests.get(url, timeout=timeout)
        return r.json()
    except requests.exceptions.ConnectTimeout as e:
        LOG.warn("Failed to load VARS concepts, check network")
        return None

