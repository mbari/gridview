# VARS and gridview

To use griddview with VARS:

```bash
# If you have SSH keys for bitbucket configured
git clone git@bitbucket.org:mbari/gridview.git

# Alternatively you can check it out with no login credentials
git clone https://bitbucket.org/mbari/gridview.git


cd gridview
git checkout vars_integration 
```

Then follow the instructions in the README.md file for installing the correct python dependencies.

## Configuration

### API KEY

An API key is required so that gridview has permissions to modify data in M3/VARS. To configure the key:

- Contact Brian Schlining (brian@mbari.org) for the API key.
- open or create `gridview/config/api_key.txt` and paste the key into the file.

### Database configuration

Gridview needs read-access to the annotations database. To configure the database:

- Contact Brian Schlining (brian@mbari.org) for the _server_, password, username, and database parameters. 
- Edit `gridview/config/sources.ini` and enter the credentials. For example:

```
server = fugazi.shore.mbari.org
user = foo
password = bar
database = M3_ANNOTATIONS
```

### REST API configuration

Gridview will use REST APIs for modifying the bounding box and annotation data. To configure the _anno_ endpoints:

- Contact Brian Schlining (brian@mbari.org) for the rest endpoints
- Edit `gridview/config/sources.ini`. For example:

```
; anno endpoints
anno_auth = http://nexus.shore.mbari.org:8082/anno/v1/auth
anno_associations = http://nexus.shore.mbari.org:8082/anno/v1/associations
anno_annotations = http://nexus.shore.mbari.org:8082/anno/v1/annotations
```

In general, you do not need to modify the _accounts_ or _kb_ endpoints and can leave those "as-is".

## Run

Refer to the README.md file on the python dependencies. Once those are installed, you can launch gridview using:

```bash
cd gridview
python gridview.py
```

## Image States

![Pending](assets/images/no_state.png)

__Above: Pending Verification__

![Displayed](assets/images/displayed.png)

__Above: The currently displayed localization has a blue border__

![Selected](assets/images/selected.png)

__Above: Selected localizations are green. Verifying, changing a label, or deleting are applied to all selected localizations!!__

![Verified](assets/images/verified.png)

__Localization that have been verified are yellow__


## Usage

__WARNING: There is no undo button in gridview!! Please be cautious when making bulk changes, your changes are forever.__


 
### Select/Unselect Images: ###
- Left-click on the image
- Hold down Ctrl key and drag the mouse over images to be selected

### Zoom in on selected image: ###
- Move mouse over the image detail view
- Scroll up to zoom in
- Scroll down to zoom out
- Press scroll wheel and drag to move around

### Verifying the selected images: ###
- __Make sure that the label and parts textfields are empty__
- Click the "LABEL" button
- This will add `verifier: <username>` to the bounding box information

### Apply label to all selected images: ###
- __verify that you want to apply the label to all the localizations you have selected__
- Choose the label you want to apply from the Class Label dropdown
- Click the "LABEL SELECTED" button
- Labeled images with have new label applied and will be immediately updated in the VARS database.
- If the "Hide Labeled" box is checked, the images will be removed from the grid

### Delete localizations: ###
- __verify that you want to delete all the localizations you have selected__
- Click the "DELETE" button
- Underlying localizations will be immediately deleted in VARS
The selected images will be removed from the grid

### Resize windows (grid view, full image view, and JSON view) ###
- Mouse over the bar between the views and when the pointer changes to arrows left-click and drag to change size. 
- The app should remember the state when relaunched.

### Resize a box ###
- Left-click on one of the box handles (diamond shapes) and drag while holding the mouse button down