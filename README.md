# README #

### Installation: ###
- Install Miniconda 3.6 or 3.7
- At the terminal
```bash
$ pip install lxml
$ pip install qdarkstyle
$ pip install pyqt5
$ pip install pyqtgraph
$ pip install opencv-python-headless
```

### Load a directory: ###
```bash
$ python gridview.py [PATH] 
```

example:  
```bash
python gridview.py /Users/alex/Desktop/2019_04_15_MiniROV/
```

### Grid Sorting: ###
The “Sort By” dropdown will change the way the images are sorted in the grid:
Class name: alphabetical by class name
Timestamp: alphanumeric by filename
Height: Descending by image height

### Select/Unselect Images: ###
Left-click on the image
Hold down Ctrl key and drag the mouse over images to be selected

### Zoom in on selected image: ###
Move mouse over the image detail view
Scroll up to zoom in
Scroll down to zoom out
Press scroll wheel and drag to move around

### Add more labels to the dropdown list: ###
Edit the labels.txt file with a text editor, one label per line

### Apply label to all selected images: ###
Choose the label you want to apply from the Class Label dropdown
Click the “LABEL SELECTED” button
Labeled images with have new label applied and the underlying xml file updated
If the “Hide Labeled” box is checked, the images will be removed from the grid

### Moved selected images into the folder for additional review: ###
Click the “NEEDS REVIEW” button
Underlying xml and images for the sel;ected annotations will be moved to the needs_review subdirectory of PATH
The selected images will be removed from the grid
Any related images from the same xml file will also be removed from the grid

### Discard images: ###
Click the “DISCARD” button
Underlying xml and images for the selected annotations will be moved to the discard subdirectory of PATH
The selected images will be removed from the grid
Any related images from the same xml file will also be removed from the grid

### Undo Review or Discard: ###
Click the “MOVE BACK” button to move any previously selected and moved images and xml files from either the review or discard subdirs

### Resize windows (grid view, full image view, and xml view) ###
Mouse over the bar between the views and when the pointer changes to arrows left-click and drag to change size. 
The app should remember the state when relaunched.

## Rectlabel Tool (update/add/remove annotations in full image view): ###

### Creating a new box ###
- Select the label for the new box
- Press Ctrl key
- Click on the image where you want the box to start
- Drag the mouse to where you want the box to end
- Release Ctrl key
### Deleting a box ###
- Right-click inside the box boundary and select remove ROI
- Updating a box label
- Right-click inside the box boundary and select a new label from the class label dropdown
### Resize a box ###
- Left-click on one of the box handles (diamond shapes) and drag while holding the mouse button down